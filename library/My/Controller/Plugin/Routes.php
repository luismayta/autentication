<?php

class My_Controller_Plugin_Routes extends Zend_Controller_Plugin_Abstract
{

    public function routeStartup(Zend_Controller_Request_Abstract $request) {
        $routes = array(
            'soap' => new Zend_Controller_Router_Route(
                'api/soap.wsdl',
                array(
                    'controller' => 'server',
                    'action' => 'soap-server',
                )
            )
            );


        $router = Zend_Controller_Front::getInstance()->getRouter();
        $router->addRoutes($routes);
        parent::routeStartup($request);
    }

}
?>
