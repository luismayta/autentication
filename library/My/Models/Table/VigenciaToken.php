<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class My_Models_Table_VigenciaToken
        extends Zend_Db_Table_Abstract
    {

    protected $_name = 't010_vigenciatoken';
    protected $_primary = 'NUM_TOKEN';
    protected $_primary = 'COD_RUC';
    protected $_primary = 'NOM_USUARIO';


    public function __construct()
        {
        parent::__construct();
        }


    public function RegistrarToken( $TXT_COD_RUC , $TXT_NOM_USUARIO ,
                                    $TXT_NUM_TOKEN )
        {
        $db = $this->getAdapter();
        $return;

        try
            {
            $TXT_NUM_TOKEN = 0;


            $consulta = "SELECT DISTINCT num_token, txt_num_operacion, 
                CURDATE() fec_acceso, CURTIME() txt_hora_acceso FROM ";
            $consulta .= "(SELECT num_token, 0 txt_num_operacion 
            FROM vigenciatoken WHERE num_token= '$TXT_NUM_TOKEN' ";
            $consulta .= "UNION ALL SELECT SUBSTR(CAST(RAND()*1000 AS CHAR),1,8) 
                num_token, 1 txt_num_operacion From DUAL limit 1) 
                operacion LIMIT 1";

            $result = $this->fetchRow( $consulta );

            if ( !$result || $result->txt_num_operacion === 1 )
                {
                $data = array(
                    'NUM_TOKEN' => $result->num_token ,
                    'COD_RUC' => $TXT_COD_RUC ,
                    'NOM_USUARIO' => $TXT_NOM_USUARIO ,
                    'FEC_ULTACC' => new Zend_Db_Expr( 'CURDATE()' ) ,
                    'HOR_ULTACC' => new Zend_Db_Expr( 'CURTIME()' ) ,
                    'IND_ESTADO' => '1'
                );
                $this->insert( $data );
                }
            else
                {
                $data = array(
                    'FEC_ULTACC' => new Zend_Db_Expr( 'CURDATE()' ) ,
                    'HOR_ULTACC' => new Zend_Db_Expr( 'CURTIME()' ) ,
                );

                $where['NUM_TOKEN = ?'] = $TXT_NUM_TOKEN;

                $this->update( $data , $where );
                }

            $return = "{'numToken':'$result->num_token', 
                    'fechaAcceso':'$result->fec_acceso', 
                    'horaAcceso':'$result->txt_hora_acceso'}";
            }
        catch ( Zend_Exception $exc )
            {

            $TXT_COD_ERROR = 'kusa';
            $TXT_DES_ERROR = "REGISTRANDO EL TOKEN ----- Excepcion: " . $exc->getMessage();

            $return = $TXT_COD_ERROR;
            $return = $TXT_DES_ERROR;
            $return = "{'codError': '$TXT_COD_ERROR', 'desError': '$TXT_DES_ERROR'}";
            }

        return $return;
        }


    }
