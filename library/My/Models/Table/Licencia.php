<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class My_Models_Table_Licencia
        extends Zend_Db_Table_Abstract
    {

    protected $_name = 't003_licencia';
    protected $_primary = 'COD_RUC';
    protected $_primary = 'COD_COMPROBANTE';
    protected $_primary = 'NUM_COMPROBANTE';


    public function __construct()
        {
        parent::__construct();
        }


    public function findRuc( $ruc )
        {
        $db = $this->getAdapter();
        $query = $db
                ->select()
                ->from( $this->_name ,
                        array(
                    'cod_ruc' ,
                    'cod_comprobante' ,
                    'num_comprobante'
                        )
                )
                ->where( 'COD_RUC = ?' , $ruc )
                ->where( 'FEC_INIVIG > ?' , Zend_Date::now() )
                ->where( 'FEC_FINVIG < ?' , Zend_Date::now() )
                ->query()
        ;
        return $query->fetchObject();
        }


    }
