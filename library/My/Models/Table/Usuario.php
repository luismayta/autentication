<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class My_Models_Table_Usuario
        extends Zend_Db_Table_Abstract
    {

    protected $_name = 't002_usuario';
    protected $_primary = 'COD_RUC';
    protected $_primary = 'NOM_USUARIO';


    public function __construct()
        {
        parent::__construct();
        }

/**
 *
 * @param string $ruc
 * @param string $user
 * @return object 
 */
    public function findUserByRuc( $ruc , $user )
        {
        
        $db = $this->getAdapter();
        $query = $db
                ->select()
                ->from( 'T001_empresa as e' ,
                        array(
                    'NOM_SOCIAL'
                        )
                )
                ->joinInner( 't002_usuario as u' , 
                        'u.COD_RUC = e.COD_RUC' , 
                        array(
                         'u.DES_USUARIO',
                            'u.IND_ESTADO'
                        ) 
                        )
                ->where( 'COD_RUC = ?' , $ruc )
                ->where( 'NOM_USUARIO = ?' , $user )
                ->query()
        ;
        return $query->fetchObject();
        }
   

    }
