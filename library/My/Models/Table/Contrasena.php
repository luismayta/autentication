<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class My_Models_Table_Contrasena
        extends Zend_Db_Table_Abstract
    {

    protected $_name = 't004_contrasena';
    protected $_primary = 'COD_RUC';
    protected $_primary = 'NOM_USUARIO';
    protected $_primary = 'FEC_INVIG';


    public function __construct()
        {
        parent::__construct();
        }


    public function findUserByClave( $ruc , $user , $clave )
        {

        $db = $this->getAdapter();
        $query = $db
                ->select()
                ->from( 'T004_contrasena as c' ,
                        array(
                    'c.FEC_FINVIG' ,
                    'CURDATE() as FEC_ACTUAL'
                        )
                )
                ->where( 'COD_RUC = ?' , $ruc )
                ->where( 'NOM_USUARIO = ?' , $user )
                ->where( 'COD_CONTRASENA = ?' , $clave )
                ->query()
        ;
        return $query->fetchObject();
        }


    }
