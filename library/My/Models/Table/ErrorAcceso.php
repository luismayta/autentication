<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class My_Models_Table_ErrorAcceso
        extends Zend_Db_Table_Abstract
    {

    protected $_name = 't011_erroracceso';


    public function __construct()
        {
        parent::__construct();
        }


    public function RegistrarError( $TXT_COD_ERROR , $TXT_DES_ERROR )
        {

        $db = $this->getAdapter();

        try
            {

            $data = array(
                'FEC_OPERACION' => new Zend_Db_Expr( 'CURDATE()' ) ,
                'HOR_OPERACION' => new Zend_Db_Expr( 'CURTIME()' ) ,
                'COD_ERROR' => $TXT_COD_ERROR ,
                'DES_ERROR' => $TXT_DES_ERROR
            );
            $this->insert( $data );

            return "{'codError': '$TXT_COD_ERROR', 'desError': '$TXT_DES_ERROR'}";
            }
        catch ( Zend_Exception $exc )
            {

            return "{'codError': '$TXT_COD_ERROR', 'desError': '$TXT_DES_ERROR-$exc'}";
            }
        }


    }
