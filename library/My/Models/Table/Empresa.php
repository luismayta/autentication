<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class My_Models_Table_Empresa
        extends Zend_Db_Table_Abstract
    {

    protected $_name = 't001_empresa';
    protected $_primary = 'COD_RUC';


    public function __construct()
        {
        parent::__construct();
        }


    public function findRuc( $ruc )
        {
        $db = $this->getAdapter();
        $query = $db
                ->select()
                ->from( $this->_name )
                ->where( 'cod_ruc = ' . $ruc )
                ->query()
        ;
        return $query->fetchObject();
        }


    


    }
