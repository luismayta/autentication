<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class My_Models_Table_Acceso
        extends Zend_Db_Table_Abstract
    {

    protected $_name = 't008_acceso';
    protected $_primary = 'COD_RUC';
    protected $_primary = 'NOM_USUARIO';
    protected $_primary = 'FEC_ACCESO';


    public function __construct()
        {
        parent::__construct();
        }


    public function registrarAcceso( $TXT_COD_RUC , $TXT_NOM_USUARIO )
        {
        $db = $this->getAdapter();
        $result = null;
        try
            {

            $TXT_NUM_OPERACION = null;

            $consultaInformacionAcceso = "SELECT DISTINCT fec_acceso,
                    max(num_operaciones) txt_num_operacion,
                    CURTIME() txt_hora_acceso ";
            $consultaInformacionAcceso.= "FROM (SELECT fec_acceso, 
                num_operaciones FROM acceso WHERE cod_ruc='$TXT_COD_RUC' ";
            $consultaInformacionAcceso.= "AND nom_usuario='$TXT_NOM_USUARIO'
                AND fec_acceso=CURDATE() UNION ALL SELECT ";
            $consultaInformacionAcceso.= "CURDATE() fec_acceso,
                    1 num_operaciones From DUAL limit 1) 
                    operacion ORDER BY fec_acceso, num_operaciones";

            $result = $db->fetchRow( $consultaInformacionAcceso );
            if ( $result == NULL
                    ||
                    count( $result ) === 0
                    ||
                    $result->txt_num_operacion === 1 )
                {
                $data = array(
                    'COD_RUC' => $TXT_COD_RUC ,
                    'NOM_USUARIO' => $TXT_NOM_USUARIO ,
                    'FEC_ACCESO' => new Zend_Db_Expr( 'CURDATE()' ) ,
                    'HOR_ULTACC' => new Zend_Db_Expr( 'CURTIME()' ) ,
                    'NUM_OPERACIONES' => '1' //Cambiar por 0 para pruebas
                );
                $this->insert( $data );
                }
            else
                {
                $data = array( 'HOR_ULTACC' => new Zend_Db_Expr( 'CURTIME()' ) );

                $where['COD_RUC = ?'] = $TXT_COD_RUC;
                $where['NOM_USUARIO = ?'] = $TXT_NOM_USUARIO;
                $where['FEC_ACCESO = ?'] = new Zend_Db_Expr( 'CURDATE()' );

                $this->update( $data , $where );
                }



            $return = "{'fecAcceso':'$result->fec_acceso',
                    'horaAcceso':'$result->txt_hora_acceso'}";
            }
        catch ( Zend_Exception $exc )
            {

            $TXT_COD_ERROR = 'Error';
            $TXT_DES_ERROR = "REGISTRANDO EL ACCESO ----- Excepcion: "
                    . $exc->getMessage();

            $return = "{'codError': '$TXT_COD_ERROR',
                    'desError': '$TXT_DES_ERROR'}";
            }

        return $return;
        }


    }
