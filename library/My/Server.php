<?php


/**
 * Web service methods
 */
class My_Server
    {


    /**
     * Get the server date and time
     *
     * @return  string
     */
    public function getDate()
        {
        $date = new Zend_Date();
        return $date->now()->__toString();
        }


    /**
     * conseguirla edad de 2 parametros
     *
     * @param   string  $name   The name of a person
     * @param   int     $age    The age of a person
     * @return  string
     */
    public function getAgeString( $name , $age )
        {
        return sprintf( '%s is %d years old' , $name , $age );
        }


    /**
     * devuelve la fila del usuario
     * 
     * @param string $name  el nombre del usuario
     * @return string    la fila del usuario
     */
    public function getNameUsuario( $name )
        {
        $usuario = new Application_Model_Usuario();
        return $usuario->getName( $name );
        }


    /**
     * valida el ruc ingresado
     * 
     * @param string $ruc 
     * @return string
     */
    public function validateRuc( $ruc )
        {
        $empresa = new My_Models_Table_Empresa();
        $response = $empresa->findRuc( $ruc );
        if ( !is_null( $response ) )
            {
            return $response;
            }
        else
            {
            $response = "RUC de empresa $ruc no es válido";
            return $response;
            }
        }


    /**
     * validar la licencia de empresa
     * @param string $ruc
     * @return string 
     */
    public function validarLicenciaEmpresa( $ruc )
        {
        $licencia = new My_Models_Table_Licencia();
        $response = $licencia->findRuc( $ruc );
        if ( !is_null( $response ) )
            {
            return $response;
            }
        else
            {
            $response = "RUC de empresa $ruc no cuenta con licencia vigente";
            return $response
            ;
            }
        }


    /**
     * valida el usuario por empresa
     * @param string $ruc
     * @param string $nomusuario
     * return string 
     */
    public function validarUsuarioByEmpresa( $ruc , $nomusuario )
        {
        $usuario = new My_Models_Table_Usuario();
        $response = $usuario->findUserByRuc( $ruc , $nomusuario );
        if ( !is_null( $response ) )
            {
            return $response;
            }
        else
            {
            $response = "Usuario $nomusuario no es válido o 
            no se encuentra vigente";
            return $response;
            }
        }


    /**
     * validar la clave por usuario
     * @param string $ruc
     * @param string $user
     * @param string $clave 
     * return string
     */
    public function validarClaveByUsuario( $ruc , $user , $clave )
        {
        $contrasena = new My_Models_Table_Contrasena();
        $response = $contrasena->findUserByClave( $ruc , $user , $clave );
        if ( !is_null( $response ) )
            {
            return $response;
            }
        else
            {
            $response = "Contraseña no es válida para el Usuario 
                $user 
                de la empresa 
                $ruc";
            return $response;
            }
        }


    public function registrarAcceso( $TXT_COD_RUC , $TXT_NOM_USUARIO )
        {
        $db = $dbWS->getAdapter();
        $result = null;
        $return = new Resultado();

        try
            {
            $db->getConnection();
            if ( $db->isConnected() )
                {
                $db->setFetchMode( Zend_Db::FETCH_OBJ );

                $TXT_NUM_OPERACION = null;

                $consultaInformacionAcceso = "SELECT DISTINCT fec_acceso, max(num_operaciones) txt_num_operacion, CURTIME() txt_hora_acceso ";
                $consultaInformacionAcceso.= "FROM (SELECT fec_acceso, num_operaciones FROM acceso WHERE cod_ruc='$TXT_COD_RUC' ";
                $consultaInformacionAcceso.= "AND nom_usuario='$TXT_NOM_USUARIO' AND fec_acceso=CURDATE() UNION ALL SELECT ";
                $consultaInformacionAcceso.= "CURDATE() fec_acceso,1 num_operaciones From DUAL limit 1) operacion ORDER BY fec_acceso, num_operaciones";

                $result = $db->fetchRow( $consultaInformacionAcceso );
                if ( $result == NULL || count( $result ) == 0 || $result->txt_num_operacion == 1 )
                    {
                    $data = array(
                        'COD_RUC' => $TXT_COD_RUC ,
                        'NOM_USUARIO' => $TXT_NOM_USUARIO ,
                        'FEC_ACCESO' => new Zend_Db_Expr( 'CURDATE()' ) ,
                        'HOR_ULTACC' => new Zend_Db_Expr( 'CURTIME()' ) ,
                        'NUM_OPERACIONES' => '1' //Cambiar por 0 para pruebas
                    );
                    $db->insert( 'acceso' , $data );
                    }
                else
                    {
                    $data = array( 'HOR_ULTACC' => new Zend_Db_Expr( 'CURTIME()' ) );

                    $where['COD_RUC = ?'] = $TXT_COD_RUC;
                    $where['NOM_USUARIO = ?'] = $TXT_NOM_USUARIO;
                    $where['FEC_ACCESO = ?'] = new Zend_Db_Expr( 'CURDATE()' );

                    $db->update( 'acceso' , $data , $where );
                    }

                $db->closeConnection();
                $return->casoError_Conformidad = false;
                $return->cadenaResultado = "{'fecAcceso':'$result->fec_acceso', 'horaAcceso':'$result->txt_hora_acceso'}";
                }
            }
        catch ( Zend_Exception $exc )
            {
            if ( $db->isConnected() ) $db->closeConnection();
            $TXT_COD_ERROR = 'RAMER';
            $TXT_DES_ERROR = "REGISTRANDO EL ACCESO ----- Excepcion: " . $exc->getMessage();
            $return->casoError_Conformidad = true;
            $return->TXT_COD_ERROR = $TXT_COD_ERROR;
            $return->TXT_DES_ERROR = $TXT_DES_ERROR;
            $return->cadenaResultado = "{'codError': '$TXT_COD_ERROR', 'desError': '$TXT_DES_ERROR'}";
            }

        return $return;
        }


    }
