<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class My_Form_Validacion
        extends Zend_Form
    {


    public function init()
        {
        $this
                ->setMethod( 'post' )
                ->setAttrib( 'id' , 'frmValidacion' )
        ;

        $codigo = new Zend_Form_Element_Text( 'cod_ruc' );
        $codigo
                ->setLabel( 'TXT_COD_RUC' )
                ->addValidator( new Zend_Validate_StringLength(
                                array( 'min' => 11 , 'max' => 11 )
                        )
                )
                //->addValidator( new Zend_Validate_NotEmpty() )
                ->setRequired();
        ;

        $usuario = new Zend_Form_Element_Text( 'nom_usuario' );
        $usuario->setLabel( 'TXT_NOM_USUARIO' )
                ->setRequired()
                //->addValidator( new Zend_Validate_Alnum() )
        ;

        $clave = new Zend_Form_Element_Password( 'cod_contrasena' );
        $clave->setLabel( 'TXT_COD_CONTRASENA' )
                ->setRequired();

        ;

        $submit = new Zend_Form_Element_Submit( 'submit' );
        $submit->setLabel( 'Enviar' );

        $this->addElements(
                array( $codigo , $usuario , $clave , $submit )
        );
        }


    }

?>
