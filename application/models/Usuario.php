<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Usuario
 *
 * @author Administrador
 */
class Application_Model_Usuario extends Zend_Db_Table_Abstract
    {

    protected $_name = "t002_usuario";

    public function showAll()
        {
        return $this->fetchAll( Zend_Db::FETCH_OBJ );
        }

    public function getName( $nameUser )
        {
        if ( empty( $nameUser ) )
            {
            return "";
            }

        return $this->fetchRow( $this->select()->where( 'NOM_USUARIO=?' , $nameUser ) );
        }

    }
