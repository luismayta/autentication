<?php

class ServerController extends My_Controller_Action
    {

    public function init()
        {
        /* Initialize action controller here */
        }

    public function indexAction()
        {
        
        }

    public function soapServerAction()
        {

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $class_service = "My_Server";

        if ( 'GET' == $_SERVER['REQUEST_METHOD'] )
            {
            $autodiscover = new Zend_Soap_AutoDiscover();
            $autodiscover->setClass( $class_service );
            $autodiscover->handle();
            } else
            {
            $soap = new Zend_Soap_Server();
            $soap->setClass( $class_service );
            $soap->setUri( 'http://authenticacion.zend.local/api/soap.wsdl' );
            $soap->handle();
            }
        }

    public function clientAction()
        {

        $client = new Zend_Soap_Client( 'http://authenticacion.zend.local/api/soap.wsdl' );
        $this->view->r1 = $client->getNameUsuario('luis');
        }

    }